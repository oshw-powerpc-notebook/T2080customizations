List of viable solution for (cross)compiling u-boot
- Method 1 - Cross-compile u-boot sourced on Ubuntu 22.04
- Method 2 - Using NXP toolchain (tested on Ubuntu 14.04 and 16.04)
- Method 3 - Using DENX ELDK toolchain

------

# Method 1 - Cross-compile u-boot sourced on Ubuntu 22.04

### Required software packages
```
$ sudo apt update && sudo apt dist-upgrade
$ sudo apt install build-essential crossbuild-essential-powerpc git libncurses-dev bison flex libssl-dev
```
### Clone locally the official u-boot sources
```
$ git clone https://source.denx.de/u-boot/u-boot
```
Alternatively you may want to try the original Freescale u-boot sources
```
$ git clone https://gitlab.com/power-progress-community/oshw-powerpc-notebook/freescale.com-ppc-u-boot
```
### Create a configuration file from a template
```
$ cd u-boot
$ make ARCH=powerpc CROSS_COMPILE=powerpc-linux-gnu- T2080RDB_SDCARD_defconfig
```
### Customize the configuration
```
$ make ARCH=powerpc CROSS_COMPILE=powerpc-linux-gnu- menuconfig
```
### Build the binary file u-boot
```
make ARCH=powerpc CROSS_COMPILE=powerpc-linux-gnu-
```
At the end of the compilation the following warning message appears
```
===================== WARNING ======================
This board does not use CONFIG_DM_SERIAL (Driver Model
for Serial drivers). Please update the board to use
CONFIG_DM_SERIAL before the v2023.04 release. Failure to
update by the deadline may result in board removal.
See doc/develop/driver-model/migration.rst for more info.
====================================================
```
### Prepare the SD card
Insert your SD card in your card reader attached to a PC with Linux.<br>
Using gparted (Debian/Ubuntu users: if not present you may install it entering “sudo apt install gparted”)

1. Select the SD card from the list of available disk devices and keep trace of the device name (in our case was “/dev/sdc”)
2. Create a new msdos partition table for the SD card
3. Create one primary “ext2” partition leaving 1GB of empty space BEFORE the primary partition and mark such as “ext2” partition as bootable.
Specifying an ext2 filesystem is important, otherwise U-Boot might not be able to read its content.
4. Apply all changes to the disk
5. Close gparted

### Write u-boot to the SD card
In the following command replace "/dev/sdc" with the correct sd card device, writing the a wrong device might harm you computer, be careful!
```
sudo dd if=u-boot.bin of=/dev/sdc seek=8 count=2048
```


# Method 2 - Using NXP toolchain (tested on Ubuntu 14.04 and 16.04)
The NXP toolchain was tested on Ubuntu 22.04, 20.04, and 18.04 for both 32bit (x86) and 64bit (x86_64) but on these systems it has problems.<br>
I was tested successfully on Ubuntu 16.04 (only warns about unsupported platform) and 14 04 (no warnings), both 32bit (x86) and 64bit (x86_64) versions.

### Update the system and install basic libraries
```
$ sudo apt update && sudo apt dist-upgrade
$ sudo apt install build-essential python2.7 python2.7-dev diffstat gawk chrpath libsdl1.2-dev texi2html git libncurses-dev bison flex libssl-dev
```
### Download the NXP Linux SDK 2.0 Source and Cache ISOs from Linux SDK for QorIQ Processors

Please note that in order to download the NXP software you must register and agree to the NXP terms of use.
Download these files:
1) QorIQ Linux SDK v2.0 SOURCE.iso
2) QorIQ Linux SDK v2.0-1703.tar.bz2
3) QorIQ Linux SDK v2.0 PPCE6500 Yocto Cache.iso (32 bit)
4) QorIQ Linux SDK v2.0 PPC64E6500 Yocto Cache.iso (64 bit)

### Mount the SDK SOURCE ISO and and run the "install" file

At the question "Where to install QorIQ-SDK-V2.0-20160527-yocto?" enter the folder you will use to crossbuild sowftare for your T2080RDB.<br>
The default is the home directory that runs the installer. In may case I have the linux user "ppc64" and I entered "/home/ppc64/Documents".<br>
When the installation ends, few manual steps remains to be performed, they all listed at the end of the installation
In my case appeared the following:
```
$ cd /home/ppc64/Documents/QorIQ-SDK-V2.0-20160527-yocto
Install required host pkgs to run Yocto. Use option -f to run non-interactively.
$ ./sources/meta-freescale/scripts/host-prepare.sh
Get help for creating build project
$ . ./fsl-setup-env -h
```

### Run the preparation script
The first time you run the script you may encounter an error, to solve it follow the procedure at the end of the message:
```
Verifying sudo permission to execute apt-get command.
I ran the command: sudo -S -l which returned:

[sudo] password for ppc64: Sorry, try again.
[sudo] password for ppc64: Sorry, try again.
[sudo] password for ppc64: sudo: 3 incorrect password attempts

This means you don't have sudo permission without password to execute
apt-get command as root. This is needed to install host packages correctly.

To configure this, as root using the command "/usr/sbin/visudo",
and add the following line in the User privilege section:

ppc64 ALL = NOPASSWD: /usr/bin/apt-get
```
After that, re-run the preparation script, it now should works without errors and a series of additional required components will be downloaded and installed in your system.

### Proceed installing the Yocto Cache ISO, choose between the 32 bit or the 64 bit version
Mount and run the "install" script.<br>
When the script request "Where to install QorIQ-SDK-V2.0-20160527-yocto?", provide exactly the same folder you provided when installing the content of the previous ISO.<br>
Run the preparation script, just in case anything required is still missing from your system.<br>
Open a terminal and enter the folder "QorIQ-SDK-V2.0-20160527-yocto" and configure the system to build software for the T2080RDB by entering

## Patch the SKD to v2.0-1703
Extract the content of the file "QorIQ Linux SDK v2.0-1703.tar.bz2" entering
```
$ tar -xf "QorIQ Linux SDK v2.0-1703.tar.bz2"
```
Enter the folder where you extracted the tar.bz2 file and run the "install" script, when requested enter the folder where you installed the SDK, in my case it is "/home/ppc64/Documents/QorIQ-SDK-V2.0-20160527-yocto".

### Prepare the development environment for the T2080RDB
For the 32 bit version enter
```
$ source ./fsl-setup-env -m t2080rdb
```
Fot the 64 bit version enter
```
$ source ./fsl-setup-env -m t2080rdb-64b
```
### Get the u-boot source code
In the terminal enter
```
$ bitbake u-boot -c cleansstate
```
And patch its sources by entering
```
$ bitbake u-boot -c patch
```
### Customize the u-boot configuration
The source code of u-boot is in the following folder
```
$ cd tmp/work/t2080rdb-fsl-linux/u-boot-qoriq/2016.01+fslgit-r0/git
```
### Compile the u-boot source code

Go back to folder build_t2080rdb to rebuild u-boot
```
$ bitbake u-boot
```
The output u-boot binary files for the 32 bit version are stored in the folder
```
$ ls -la tmp/deploy/images/t2080rdb/
```
Whereas when compiling the 64 bit version are in the folder
```
$ ls -la tmp/deploy/images/t2080rdb-64b/
```
### Write the u-boot binary into the SD card
Write u-boot to the sd card  (in our case /dev/sdb) [**WARNING: entering the wrong device might harm your computer**]
```
$ sudo dd if=u-boot-sdcard.bin  of=/dev/sdb bs=512 seek=8
```

If you need to wipe out the entire SD card (in our case /dev/sdb) you may use [**WARNING: entering the wrong device might harm your computer**]
```
dd if=/dev/zero of=/dev/sdb bs=1M count=1
```


# Method 3 - Using DENX ELDK toolchain

Download and install the ELDK v5.6 ISO for PowerPC at ftp://ftp.denx.de/pub/eldk/5.6/iso/eldk-5.6-powerpc.iso
